package intranet.client.android.delegates;

import java.utils.List;

import intranet.client.android.network.model.Announcement;

public interface RetrieveAnnouncementsDelegate {
    void onAnnouncementsFetched(List<Announcement> announcements);
}
