package intranet.client.android.delegates;

import intranet.client.android.network.model.Announcement;

public interface AnnouncementAdapterDelegate {
    void onAnnouncementTapped(Announcement announcement);
}
